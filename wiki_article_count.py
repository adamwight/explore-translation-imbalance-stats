import csv
from csv_cache import cached
from io import StringIO
import re
import requests


url = "https://gitlab.com/wmde/technical-wishes/wiki-article-counter/-/raw/main/wiki_article_counts.csv"


@cached("wiki_article_count.csv")
def get_wiki_counts():
	result = requests.Session().get(url)
	reader = csv.DictReader(StringIO(result.text))
	return [row for row in reader]


def normalize_lang(name):
    name = re.sub(r"wiki$", "", name)
    name = re.sub(r"_", "-", name)
    return name


def get_language_sizes():
	raw = get_wiki_counts()
	return {normalize_lang(row["wiki"]): int(row["article_count"]) for row in raw}
