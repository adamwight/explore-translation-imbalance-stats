# Explore translation imbalance stats

**TODO: Add description**

## Install dependencies

```
pip install -r requirements.txt
```

## Run the notebook

```
jupyter notebook correlate-with-size.ipynb
```

## Open Orange workflow

```
orange-canvas explore.ows
```

## Running unit tests

Some of the complex functions can be tested from the command line:

```
pytest
```
