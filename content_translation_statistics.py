from collections import defaultdict
from csv_cache import cached
import requests


@cached("content_translation_stats.csv")
def get_stats():
	return requests.Session().get(url="https://en.wikipedia.org/w/api.php", params={
			"action": "query",
			"list": "contenttranslationstats",
			"format": "json"
	}).json()["query"]["contenttranslationstats"]["pages"]


def get_published():
	return [pair
	 for pair in get_stats()
	 if pair["status"] == "published"]


def get_translated_out():
	translated_out = defaultdict(int)
	for pair in get_published():
			translated_out[pair["sourceLanguage"]] += int(pair["count"])
	
	return translated_out
